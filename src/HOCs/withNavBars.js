import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import {
  TopNavBar,
  SideNavBar
} from "../components";
import Grid from "@mui/material/Grid";

// material UI styles
const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    overflow: "hidden",
  },
  content: {
    // height: "calc(100% - 64px)",
    minHeight: '100vh',
    overflow: "auto",
    width: "100%",
    padding:'10px 60px',
    background: '#f4f4f7',
    backgroundColor: theme.palette.primary.contrastText,
    [theme.breakpoints.down("sm")]: {
      padding:'10px',
    },
  },
  topNavbar: {
    position: "relative",
  },
  sideNavbar: {
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  positionrelative: {
    position: "relative",
  },
}));
// end

const withNavBars = (Component) => (props) => {

  // use classes
  const classes = useStyles({ props });
  // end

  return (
    <div className={classes.root}>
      {/* Your nav bars here */}

      <Grid container>

        <Grid
          item xs={12} md={2.7} lg={1.8}
          className={classes.positionrelative}
        >

          {/* sidebar */}
          {
            <div className={classes.sideNavbar}>
              <SideNavBar />
            </div>
          }
        </Grid>
        {/* end */}

        <Grid item xs={12} md={9.3} lg={10.2}>

          {/* topbar */}

          <div className={classes.topNavbar}>
            <TopNavBar />
          </div>

          {/* end */}

          {/* Content */}
          <div className={classes.content}>
            <Component {...props}>{props.children}</Component>
          </div>
          {/* end */}

        </Grid>

      </Grid>

    </div>
  );
};

export default withNavBars;