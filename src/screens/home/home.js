import React from "react";
import {
  Grid,
  Typography,
  Box,
  FormControl,
  NativeSelect,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { RelatedCards, Countries, AuthorsCard } from "../../utils";
import {
  WelcomeCard,
  CarouselSliders,
  ActivityCard,
  BasicTable,
  Card,
  CardSection,
  CircleChart,
  LineCharts,
} from "../../components";
// material ui
const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    overflow: "auto",
    paddingBottom: "100px",
  },
  chartMain: {
    padding: "30px",
    boxShadow: "0 27px 43px 0px #f2f3f5",
    border: "1px solid #f2f3f5",
    borderRadius: "12px",
  },
  tittle: {
    fontWeight: 600,
    fontSize: "18px",
    marginBottom: "30px",
  },
  tittle1: {
    fontWeight: 600,
    fontSize: "18px",
    marginBottom: "10px",
  },
  percentage: {
    color: "#7B8995",
    fontSize: "16px",
    marginBottom: "30px",
  },
  dot: {
    height: "10px",
    width: "10px",
    borderRadius: "50%",
    backgroundColor: "green",
    margin: "6px",
  },

  dot1: {
    height: "10px",
    width: "10px",
    borderRadius: "50%",
    backgroundColor: "yellow",
    margin: "6px",
  },
}));
const data = [
  {
    img: "https://media.istockphoto.com/photos/confident-smiling-indian-young-woman-professional-student-customer-picture-id1181196036?k=20&m=1181196036&s=612x612&w=0&h=LZ-nBrRb2Z95zAZ-I882snf1bxFePsebtwGApVSJrbQ=",
    tittle: "FEATURED APP",
    des: "Lightroom mobile - Koloro",
    con: "Apply these 7 secret Thecnology",
  },
  {
    img: "https://media.istockphoto.com/photos/productivity-powered-by-digital-technology-picture-id1330965067?b=1&k=20&m=1330965067&s=170667a&w=0&h=ys_MV3zYkn2HJCtHC4s_03Sz1MUC2BZv6PcDdk__XSc=",
    tittle: "FEATURED APP",
    des: "Lightroom mobile - Koloro",
    con: "Apply these 7 secret Thecnology",
  },
  {
    img: "https://media.istockphoto.com/photos/excited-happy-indian-girl-celebrating-online-win-holding-phone-picture-id1188562868?k=20&m=1188562868&s=612x612&w=0&h=QHvJ0Jo8q5un_R0gbZ7ZgO__qrKzUrhro2-olhLcfKs=",
    tittle: "FEATURED APP",
    des: "Lightroom mobile - Koloro",
    con: "Apply these 7 secret Thecnology",
  },
];

export const Home = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        {/* welcome card */}
        <Grid item xs={12} sm={12} md={12} lg={8}>
          <WelcomeCard />
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={4}>
          {/* slider */}
          <CarouselSliders data={data} autoScroll={true} onClick={true} />
        </Grid>
      </Grid>
      {/* activity card */}

      {/* Total Active Users  */}
      <Grid container spacing={3} style={{ marginTop: "10px" }}>
        <Grid item lg={4} md={6} sm={6} xs={12}>
          <ActivityCard
            isStack={true}
            color={"#26B86E"}
            tittle={"Total Active Users"}
            data={"+2.6"}
            total={"18,765"}
          />
        </Grid>
        <Grid item lg={4} md={6} sm={6} xs={12}>
          {/* Total Installed */}
          <ActivityCard
            isStack={true}
            color={"#4CA8FF"}
            tittle={"Total Installed"}
            data={"+0.2"}
            total={"4,876"}
          />
        </Grid>
        <Grid item lg={4} md={6} sm={6} xs={12}>
          {/* Total Downloads */}
          <ActivityCard
            isStack={false}
            color={"#FF825C"}
            tittle={"Total Downloads"}
            data={"-0.2"}
            total={"678"}
          />
        </Grid>
      </Grid>

      {/* charts */}
      <Grid container spacing={3} style={{ marginTop: "10px" }}>
        <Grid item xs={12} sm={12} md={12} lg={5}>
          {/* CircleChart */}
          <div className={classes.chartMain}>
            <Typography className={classes.tittle} variant="h6">
              Current Download
            </Typography>
            <div>
              <CircleChart />
            </div>
            <Box>
              <Box className={classes.catagery}>
                <Box
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    marginTop: "60px",
                    paddingTop:'20px',
                    borderTop:'1px solid #f2f3f5'
                  }}
                >
                  {/* options */}
                  <Box
                    className={classes.dot}
                    style={{ marginRight: "10px", backgroundColor: "#C8FACD" }}
                  />
                  <span>Mac</span>
                  <Box
                    className={classes.dot}
                    style={{ marginRight: "10px", backgroundColor: "#5BE584" }}
                  />
                  <span>Window</span>
                  <Box
                    className={classes.dot}
                    style={{ marginRight: "10px", backgroundColor: "#00AB55" }}
                  />
                  <span>IOS</span>
                  <Box
                    className={classes.dot}
                    style={{ marginRight: "10px", backgroundColor: "#007B55" }}
                  />
                  <span>Android</span>
                </Box>
              </Box>
            </Box>
          </div>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={7}>
          {/* LineCharts */}
          <div className={classes.chartMain}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {/* tittle */}
              <div>
                <Typography className={classes.tittle1} variant="h6">
                  Area Installed
                </Typography>
                <Typography className={classes.percentage} variant="h6">
                  (+43%) than last year
                </Typography>
              </div>
              <div>
                <Box sx={{ maxWidth: 120 }}>
                  <FormControl fullWidth variant="outline">
                    <NativeSelect
                      defaultValue={10}
                      // inputProps={{
                      //   name: "age",
                      //   id: "uncontrolled-native",
                      // }}
                      id="demo-customized-select-native"
                    >
                      <option value={10}>2019</option>
                      <option value={20}>2020</option>
                    </NativeSelect>
                  </FormControl>
                </Box>
              </div>
            </div>
            <Box
              style={{
                display: "flex",
                justifyContent: "right",
                marginBottom: "30px",
              }}
            >
              {" "}
              {/* options */}
              <Box className={classes.dot} />
              <span style={{ marginRight: "10px" }}>Asia</span>{" "}
              <Box className={classes.dot1} />
              <span>Africa</span>
            </Box>
            {/* chart */}
            <LineCharts />
          </div>
        </Grid>
      </Grid>

      <Grid container spacing={3} style={{ marginTop: "10px" }}>
        <Grid item xs={12} sm={12} md={12} lg={8}>
          {/* table */}
          <BasicTable />
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={4}>
          {/* Top Related Applications */}
          <Card data={RelatedCards} header={"Top Related Applications"} />
        </Grid>
      </Grid>

      <Grid container spacing={3} style={{ marginTop: "10px" }}>
        <Grid item xs={12} sm={12} md={12} lg={8}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={12} lg={7}>
              {/* op Installed Countries */}
              <Card data={Countries} header={"Top Installed Countries"} />
            </Grid>
            {/* Top Authors */}
            <Grid item xs={12} sm={12} md={12} lg={5}>
              <Card data={AuthorsCard} header={"Top Authors"} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={4}>
          {/* Conversion */}
          <CardSection
            isImg
            color={"#005249"}
            count={"38,566"}
            des={"Conversion"}
          />
          {/* Applications */}
          <div style={{ marginTop: "20px" }}>
            <CardSection
              color={"#7A4F01"}
              count={"55,566"}
              des={"Applications"}
            />
          </div>
        </Grid>
      </Grid>
    </div>
  );
};
