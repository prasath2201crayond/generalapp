import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import { Box, Typography, Button, Hidden } from "@mui/material";
// material UI styles
const useStyles = makeStyles((theme) => ({
  headertittle: {
    fontSize: "20px",
    fontWeight: 600,
    "& p": {
      margin: "0px",
      fontWeight: 600,
    },
  },
  root: {
    padding: "40px",
    backgroundColor: theme.palette.green.light,
    borderRadius: "15px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  des: {
    maxWidth: "470px",
    width: "100%",
  },
  btn: {
    backgroundColor: theme.palette.green.main,
    textTransform: "capitalize",
    borderRadius: "10px",
    "&:hover": {
      backgroundColor: theme.palette.green.main,
    },
  },
}));
// end

export const WelcomeCard = () => {
  // use classes
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Box>
        {/* tittle */}
        <Typography variant="h6" noWrap className={classes.headertittle}>
          Welcome Back,
          <p>Minimal UI!</p>
        </Typography>
        {/* description */}
        <p className={classes.des}>
          If you are going to use a passage of Lorem Ipsum, you need to be sure
          there isn't anything
        </p>
        {/* button */}
        <Button variant="contained" className={classes.btn}>
          Go Now
        </Button>
      </Box>
      <Box>
        {/* image */}
        <Hidden smDown>
          {" "}
          <img
            src="\images\main1.png"
            alt="technology"
            style={{ borderRadius: "12px" }}
          />
        </Hidden>
      </Box>
    </Box>
  );
};
