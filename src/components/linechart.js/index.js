import React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import makeStyles from "@mui/styles/makeStyles";
const data = [
  {
    name: "Jan",
    Africa: 4000,
    Asia: 2400,
    amt: 2400,
    val: 0,
  },
  {
    name: "Feb",
    Africa: 3000,
    Asia: 1398,
    amt: 2210,
    val: 30,
  },
  {
    name: "Mar",
    Africa: 2000,
    Asia: 9800,
    amt: 2290,
    val: 60,
  },
  {
    name: "Apr",
    Africa: 2780,
    Asia: 3908,
    amt: 2000,
    val: 90,
  },
  {
    name: "May",
    Africa: 1890,
    Asia: 4800,
    amt: 2181,
    val: 120,
  },
  {
    name: "Jun",
    Africa: 2390,
    Asia: 3800,
    amt: 2500,
    val: 150,
  },
  {
    name: "Jul",
    Africa: 3490,
    Asia: 4300,
    amt: 2100,
    val: 0,
  },
  {
    name: "Aug",
    Africa: 3490,
    Asia: 4300,
    amt: 2100,
    val: 0,
  },
  {
    name: "Sep",
    Africa: 3490,
    Asia: 4300,
    amt: 2100,
    val: 0,
  },
];
const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    paddingBottom:'10px'
  },
}));
export function LineCharts() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Legend />
      <ResponsiveContainer width="100%" height={400}>
        {/* linechart */}
        <LineChart
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" vertical={false} />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />

          <Line
            type="monotone"
            dataKey="Asia"
            stroke="green"
            activeDot={{ r: 8 }}
          />
          <Line type="monotone" dataKey="Africa" stroke="yellow" />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}
