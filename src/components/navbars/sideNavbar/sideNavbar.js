import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import {
  ListItemText,
  Typography,
  ListItem,
  Paper,
  Avatar,
  Box,

} from "@mui/material";
import { useHistory } from "react-router-dom";


// importing for side navbar menu listing
import { SideNavbarMenu } from "../../../utils";

// material UI styles
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    borderRadius: "0px 20px 20px 0px",
    position: "relative",
    borderRight: "1px dashed #F2F3F5",
  },
  drawer: {
    width: "100%",
    position: "relative",
    height: "100vh",
    overflow: "hidden",
    padding: "0px 20px",
  },
  avatar: {
    color: theme.palette.error.main,
    backgroundColor: theme.palette.error.light,
    float: "left",
  },
  userSection: {
    width: "100%",
    bottom: 0,
    left: 0,
    padding: "10px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    borderTop: "1px solid #8080801f",
    backgroundColor: theme.palette.primary.contrastText,
    [theme.breakpoints.up("md")]: {
      position: "absolute",
    },
  },
  menulist: {
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30,
    padding: "3px 10px",
  },
  more: {
    float: "right",
    textAlign: "center",
    marginTop: "10px",
  },
  title: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: "20px",
    paddingBottom: "14px",
  },
  footerTitle: {
    fontSize: 15,
    fontWeight: 700,
  },
  listView: {
    overflow: "auto",
    height: "79vh",
    fontSize: "15px",
    paddingBottom: "30px",
    marginTop: "10px",
    "& .MuiTypography-root": {
      marginRight: "10px",
      fontSize: "14px",
    },
    "& .MuiListItem-root": {
      display: "-webkit-box",
    },
    "& .MuiSvgIcon-root": {
      display: "flex",
    },
    "& .MuiListItem-root.Mui-selected": {
      backgroundColor: theme.palette.primary.light,
      color: theme.palette.primary.main,
      borderRadius: "12px",
      fontWeight: 600,
      "& .MuiTypography-root": {
        fontWeight: 600,
        fontSize: "14px",
      },
    },
  },
  menuIcon: {
    minWidth: "auto",
    padding: 8,
  },
  titleName: {
    fontSize: "15px",
    fontWeight: 600,
  },
  headertittle: {
    fontSize: "14px",
    fontWeight: 600,
    marginLeft: "15px",
    "& p": {
      margin: "0px",
      fontWeight: 500,
      color: "#84919C",
    },
  },
}));
// end

export const SideNavBar = (props) => {
  // use classes
  const classes = useStyles(props);
  // end

  // use location history
  const history = useHistory();
  // end



  const isSelected = (data) => {
    if (data.link) {
      return history.location.pathname === data.link;
    }
  };

  const onListClick = (data) => {
    if (data.link) {
      history.push(data.link);
    }
  };

  // end

  return (
    <div className={classes.root}>
      <Paper className={classes.drawer} square>
        {/* sidebar title */}
        <Typography variant="h6" className={classes.title} noWrap>
          <img
            src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRhUf_q7p5OOMOCgU1_GdeNnbWW0RtKpW05Kh_NJujvJgCG5bLo"
            alt="icon"
            width="20%"
            height="20%"
          />
          <img
            src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRJ-Liou-eEdsVUCn4kM8EqX0w6kEVbiGwdv2wzjuFmuT_7-67p"
            alt="icon"
            width="15%"
            height="15%"
            style={{ cursor: "pointer" }}
            onClick={props?.toogleSideNavBar}
          />
        </Typography>
        {/* admin box */}
        <Box
          style={{
            display: "flex",
            alignItems: "center",
            backgroundColor: "#F2F3F5",
            padding: "16px",
            borderRadius: "15px",
            margin: "6px 0px 35px",
          }}
        >
          <div>
            <Avatar
              alt="Remy Sharp"
              src="https://res.cloudinary.com/minimal-ui/image/upload/v1614655910/upload_minimal/avatar/minimal_avatar.jpg"
            />
          </div>
          <div>
            <Typography variant="h6" noWrap className={classes.headertittle}>
              Minimal UI
              <p>admin</p>
            </Typography>
          </div>
        </Box>
        {/* end */}
        <Typography variant="h6" className={classes.titleName} noWrap>
          General
        </Typography>
        <div className={classes.listView}>
          {/* to looping sidebar menulisting */}
          {SideNavbarMenu.map((navBar, index) => (
            <ListItem
              className={classes.menulist}
              onClick={(e) => onListClick(navBar)}
              button
              key={index}
              selected={isSelected(navBar)}
            >
              <Box
                style={{
                  display: "flex",
                  margin: "5px 0px",
                  alignItems: "center",
                }}
              >
                {/* icons */}
                <ListItemText primary={navBar?.icon} className={classes.menu} />

                {/* title */}
                <ListItemText primary={navBar.tittle} />
              </Box>
              {/* end */}
            </ListItem>
          ))}
          {/* end */}
        </div>
      </Paper>
    </div>
  );
};
