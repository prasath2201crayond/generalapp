import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import {Avatar, Box } from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import { Drawer } from "@mui/material";
import { SideNavBar } from "..";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";

// material UI styles
const useStyles = makeStyles((theme, props) => ({
  grow: {
    flexGrow: 1,
    zIndex: theme.zIndex.drawer + 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "transparent",
    boxShadow: "none",
    padding: "15px 0px",
  },
  title: {
    display: "block",
    color: theme.palette.text.main,
  },
  titleContainer: {
    marginLeft: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginLeft: theme.spacing(0),
    },
    "& .MuiTypography-root": {
      [theme.breakpoints.down("sm")]: {
        fontSize: "14px",
      },
    },
  },
  menuIcon: {
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  search: {
    "& .MuiOutlinedInput-root": {
      borderRadius: "12px",
      backgroundColor: "#0f2b79 !important",
      color: theme.palette.primary.contrastText,
      paddingRight: "0px",
    },
  },
  searchmd: {
    margin: "0px 10px",
    "& .MuiOutlinedInput-root": {
      borderRadius: "12px",
      backgroundColor: "#0f2b79 !important",
      color: theme.palette.primary.contrastText,
      paddingRight: "0px",
    },
  },
  createButton: {
    margin: "10px",
    textTransform: "capitalize",
    color: theme.palette.primary.contrastText,
    border: "1px solid",
    borderColor: theme.palette.primary.dark,
    padding: "4px 8px",
  },

  dropdownbutton: {
    padding: 10,
    cursor: "pointer",
    marginLeft: "3px",
    display: "flex",
    opacity: (props) => (props.action ? 1 : 0.7),
    [theme.breakpoints.down("sm")]: {
      fontSize: "14px",
      justifyContent: "center",
    },
  },
  filter: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "0px 30px",
    cursor: "pointer",
    "& .MuiTypography-root": {
      marginLeft: "8px",
    },
  },
  notfication: {
    position: "absolute",
    backgroundColor: "red",
    borderRadius: "50%",
    fontSize: "12px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "20px",
    height: "20px",
    fontWeight:600,
    top:'-6px',
    right:'-7px',
  },
  options:{
      '&:hover':{
        transition:' width 2s',
        width:'40px',
        height:'40px',
        borderRadius: "50%",
        display:'flex',
        justifyContent: "center",
        alignItems: "center",
        cursor:'pointer',
        backgroundColor: '#ecf0f3',
      }
  }
}));
// end

export const TopNavBar = (props) => {
  // use classes
  const classes = useStyles(props);
  // end

  // to maintain state
  const [state, setState] = React.useState({
    openSideNavBar: false,
  });
  // end

  // finctions start
  // side nav bar
  const toogleSideNavBar = () => {
    setState({
      ...state,
      openSideNavBar: !state.openSideNavBar,
    });
  };

  // end
  return (
    <div className={classes.grow}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <IconButton
            className={classes.menuIcon}
            onClick={toogleSideNavBar}
            size="large"

          >
            <MenuIcon htmlColor="#0f2b79"/>
          </IconButton>

          {/* topnavbar tittle */}
          <div className={classes.titleContainer}>
            <SearchIcon className={classes.title} />
          </div>

          <div className={classes.grow} />

          <Box style={{ display: "flex", alignItems: "center" }}>
            <div className={classes.options}>
              {/* flag */}
              <img
                src="https://minimal-assets-api.vercel.app/assets/icons/ic_flag_en.svg"
                alt="flag"
              />
            </div>
            {/* notification */}
            <div style={{ marginLeft: "20px", position: "relative" }} className={classes.options}>
              <NotificationsIcon className={classes.title} />
              <div className={classes.notfication}>2</div>
            </div>
            {/* people icon */}
            <div style={{ marginLeft: "20px" }} className={classes.options}>
              <PeopleAltIcon className={classes.title} />
            </div>
            <div style={{ marginLeft: "20px" }} className={classes.options}>
              {/* avatar */}
              <Avatar
                alt="Remy Sharp"
                src="https://res.cloudinary.com/minimal-ui/image/upload/v1614655910/upload_minimal/avatar/minimal_avatar.jpg"
              />
            </div>
          </Box>
          <Drawer
            open={state.openSideNavBar}
            variant={"temporary"}
            anchor="left"

            onClose={toogleSideNavBar}
          >
            <div style={{ width: 320 }}>
              <SideNavBar isMobile={true} toogleSideNavBar={toogleSideNavBar}/>
            </div>
          </Drawer>
        </Toolbar>
      </AppBar>
    </div>
  );
};
