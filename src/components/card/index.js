import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import { Typography } from "@mui/material";
// material ui
const useStyles = makeStyles((theme, props) => ({
  root: {
    padding: "25px",
    backgroundColor: props?.color,
    display: "flex",
    borderRadius: "13px",
    alignItems: "center",
    position: "relative",
    [theme.breakpoints.down("sm")]: {
      padding: "25px 10px",
    },
  },
  count: {
    color: theme.palette.primary.contrastText,
    fontWeight: 600,
    fontSize: "25px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "16px",
    },
  },
  sub: {
    color: "#D9CCB6",
    fontSize: "15px",
    marginTop: "10px",
  },
  icon: {
    position: "absolute",
    right: "0",
    top: (props) => (props?.isImg ? "19px" : "30px"),
  },
}));
export const CardSection = (props) => {
  const classes = useStyles(props);
  return (
    <div className={classes.root} style={{ backgroundColor: props.color }}>
      <div>
        {/* percentage */}
        {props?.isImg ? (
          <img src="\images\load.png" alt="load" />
        ) : (
          <img src="\images\load2.png" alt="load" />
        )}
      </div>
      <div style={{ marginLeft: "10px" ,zIndex:2}}>
        <Typography variant="h1" className={classes.count}>
          {/* count */}
          {props?.count}
        </Typography>
        <Typography variant="h1" className={classes.sub}>
          {/* description */}
          {props?.des}
        </Typography>
      </div>
      <div className={classes.icon}>
        {/* back images */}
        {props?.isImg ? (
          <img src="\images\person.png" alt="person" />
        ) : (
          <img src="\images\mail1.png" alt="mail" />
        )}
      </div>
    </div>
  );
};
