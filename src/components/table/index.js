import * as React from "react";
import { styled } from "@mui/material/styles";
import { Table, Typography } from "@mui/material";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import makeStyles from "@mui/styles/makeStyles";
import { Button, Divider, Box } from "@mui/material";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import FileDownloadOutlinedIcon from "@mui/icons-material/FileDownloadOutlined";
import PrintIcon from "@mui/icons-material/Print";
import ShareIcon from "@mui/icons-material/Share";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import ArrowForwardIosOutlinedIcon from "@mui/icons-material/ArrowForwardIosOutlined";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#F4F6F8",
    color: "#637381",
    fontSize: 14,
    fontWeight: 600,
    border: 0,
    "&:nth-child(1)": {
      borderRadius: "10px 0px 0px 10px ",
    },
    "&:nth-child(5)": {
      borderRadius: "0px 10px 10px 0px ",
    },
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    backgroundColor: "#fff",
    border: 0,
    color: "#4C545D",
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  backgroundColor: "#fff",
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const useStyles = makeStyles((theme) => ({
  tittle: {
    fontWeight: 600,
    fontSize: "18px",
    marginBottom: "25px",
  },
  menu: {
    "& .MuiPopover-paper":
      {
        boxShadow: "0 0px 8px -4px #f2f3f5",
        border: "1px solid #f2f3f5",
        borderRadius: "12px",
      },
  },
  view: {
    fontWeight: 600,
    fontSize: "15px",
    display: "flex",
    alignItems: "center",
    marginRight: "55px",
    [theme.breakpoints.down("md")]: {
      marginRight: "26px",
    },
    [theme.breakpoints.down("sm")]: {
      marginRight: "26px",
    },
  },
}));

function createData(invoice, catagory, price, status, icon) {
  return { invoice, catagory, price, status, icon };
}

const rows = [
  createData(
    "INV-987654468927",
    "Mac",
    "$16.19",
    <span
      style={{
        color: "green",
        padding: "5px 10px",
        backgroundColor: "#E4F8DD",
        borderRadius: "5px",
        fontSize: "15px",
      }}
    >
      paid
    </span>,
    4.0
  ),
  createData(
    "INV-987654DDA328",
    "Mac",
    "$35.71",
    <span
      style={{
        color: "green",
        padding: "5px 10px",
        backgroundColor: "#E4F8DD",
        borderRadius: "5px",
        fontSize: "15px",
      }}
    >
      paid
    </span>,
    4.3
  ),
  createData(
    "INV-98765446V929",
    "Android",
    "$34.30",
    <span
      style={{
        color: "red",
        padding: "5px 10px",
        backgroundColor: "#FFE2E1",
        borderRadius: "5px",
        fontSize: "15px",
      }}
    >
      Out of date
    </span>,
    6.0
  ),
  createData(
    "INV-987654268920",
    "Android",
    "$93.10",
    <span
      style={{
        color: "#C19222",
        padding: "5px 10px",
        backgroundColor: "#FFF5D7",
        borderRadius: "5px",
        fontSize: "15px",
      }}
    >
      In Progress
    </span>,
    4.3
  ),
  createData(
    "INV-987654S68921",
    "Android",
    "$55.47",
    <span
      style={{
        color: "red",
        padding: "5px 10px",
        backgroundColor: "#FFE2E1",
        borderRadius: "5px",
        fontSize: "15px",
      }}
    >
      Out of date
    </span>,
    3.9
  ),
];

export function BasicTable() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const classes = useStyles();
  return (
    <TableContainer
      component={Paper}
      style={{ padding: "30px 10px", overflowX: "auto", width: "100%" }}
    >
      {/* tittle */}
      <Typography className={classes.tittle} variant="h6">
        New Invoice
      </Typography>
      {/* table start */}
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          {/* table header */}
          <TableRow>
            <StyledTableCell>Invoice ID</StyledTableCell>
            <StyledTableCell align="left">Category</StyledTableCell>
            <StyledTableCell align="left">Price</StyledTableCell>
            <StyledTableCell align="left">Status</StyledTableCell>
            <StyledTableCell align="left"></StyledTableCell>
          </TableRow>
        </TableHead>
        {/* table boady */}
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell component="th" scope="row">
                {row.invoice}
              </StyledTableCell>
              <StyledTableCell align="left">{row.catagory}</StyledTableCell>
              <StyledTableCell align="left">{row.price}</StyledTableCell>
              <StyledTableCell align="left">{row.status}</StyledTableCell>
              <StyledTableCell align="left">
                {/* drop down menu section */}
                <div>
                  <Button
                    id="demo-positioned-button"
                    aria-controls={open ? "demo-positioned-menu" : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? "true" : undefined}
                    onClick={handleClick}
                  >
                    <MoreVertIcon style={{ color: "#4C545D" }} />
                  </Button>
                  <Menu
                    style={{ boxShadow: "none !important" }}
                    anchorEl={anchorEl}
                    keepMounted
                    open={!!anchorEl}
                    onClose={handleClose}
                    getContentAnchorEl={null}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "left",
                    }}
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    className={classes.menu}
                  >
                    <MenuItem
                      onClick={handleClose}
                      style={{ color: "#212B36", fontSize: "14px" }}
                    >
                      <FileDownloadOutlinedIcon />
                      &nbsp;&nbsp;Download
                    </MenuItem>
                    <MenuItem
                      onClick={handleClose}
                      style={{ color: "#212B36", fontSize: "14px" }}
                    >
                      <PrintIcon />
                      &nbsp;&nbsp;Print
                    </MenuItem>
                    <MenuItem
                      onClick={handleClose}
                      style={{ color: "#212B36", fontSize: "14px" }}
                    >
                      <ShareIcon />
                      &nbsp;&nbsp;Share
                    </MenuItem>
                    <Divider sx={{ my: 0.5 }} />
                    <MenuItem
                      onClick={handleClose}
                      style={{ color: "#FF4842", fontSize: "14px" }}
                    >
                      <DeleteOutlinedIcon />
                      &nbsp;&nbsp;Delete
                    </MenuItem>
                  </Menu>
                </div>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
      {/* bottom of table contant */}
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <Box style={{ width: "100%" }}>
          <Divider style={{ width: "100%" }} />
          <div style={{ float: "right", marginTop: "15px" }}>
            {" "}
            <Typography className={classes.view} variant="h6">
              View All&nbsp;
              <ArrowForwardIosOutlinedIcon style={{ fontSize: "18px" }} />
            </Typography>
          </div>
        </Box>
      </Table>
    </TableContainer>
  );
}
