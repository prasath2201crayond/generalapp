import React from "react";
import { ResponsiveContainer, PieChart, Pie,Tooltip } from "recharts";
import makeStyles from "@mui/styles/makeStyles";
import { Typography } from "@mui/material";
const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.up("sm")]: {
      width:'100%',
    minWidth: "300px !important",
    height: "300px !important",
      margin: "0 auto",
    },
    "& svg": {
      borderRadius: "50%",
      width:'100%',
      minWidth: "138px !important",
      height: "138px !important",
      position: "relative",
      [theme.breakpoints.up("sm")]: {
        width:'100%',
      minWidth: "300px !important",
      height: "300px !important",
        margin: "0 auto",
      },
      "& .recharts-wrapper": {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },
  total: {
    color: "#637381",
    fontSize: "15px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "14px",
    },
  },
  des: {
    fontWeight: 600,
    color: "#252F3A",
    fontSize: "25px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "18px",
    },
  },
  center: {
    textAlign: "center",
    position: "absolute",
    left: "0px",
    right: "0px",
    top: "36%",
    [theme.breakpoints.down("sm")]: {
      left: "0px",
      right: "0px",
      top: "36%",
    },
  },
  chart: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));
const data = [
  { name: "Window", value: 300, fill: "#389D73" },
  { name: "Mac", value: 100, fill: "#C8FACD" },
  { name: "IOS", value: 500, fill: "#5BE584" },
  { name: "Android", value: 1000, fill: "#00AB55" },
];

export const CircleChart = () => {
  const classes = useStyles();
  return (
    <div style={{ position: "relative" }} className={classes.root}>
    <ResponsiveContainer
      width={111}
      height={200}
      style={{ margin: "0 auto" }}
    >
      <PieChart width={111} height={200} className={classes.chart}>
        {/* PieChart */}
        <Pie
          data={data}
          cx="50%"
          cy="50%"
          outerRadius={100}
          innerRadius={90}
          dataKey="value"
        />
        <Tooltip/>
      </PieChart>
    </ResponsiveContainer>
    <div className={classes.center}>
    <Typography className={classes.total}>Total</Typography>
        <Typography className={classes.des}>188,245</Typography>
    </div>
  </div>
  );
};
