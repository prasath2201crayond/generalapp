import React from "react";
import { Typography, Avatar, Rating ,Grid} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import AppleIcon from "@mui/icons-material/Apple";
import AdbRoundedIcon from "@mui/icons-material/AdbRounded";
// material ui
const useStyles = makeStyles((theme, props) => ({
  root: {
    padding: "20px",
    border: "1px solid #f2f3f5",
    borderRadius: "12px",
  },
  tittle: {
    fontWeight: 600,
    fontSize: "18px",
    marginBottom: "25px",
  },
  main: {
    margin: "20px 0px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  tittle1: {
    fontWeight: 600,
    fontSize: "13px",
    marginLeft: "10px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "12px",
    },
  },
  section: {
    display: "flex",
    alignItems: "center",
  },
  sub: {
    fontSize: "13px",
    color: "#637381",
    fontWeight: 600,
    marginLeft: "10px",
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      fontSize: "12px",
    },
  },
  like: {
    color: "#75838F",
    fontSize: "13px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "11px",
    },
  },
  flexBox: {
    display: "flex",
    alignItems: "center",
    marginRight: "36px",
    fontSize: "15px",
    [theme.breakpoints.down("sm")]: {
      marginRight: "7px",
      fontSize: "11px",
    },
  },
  flexBox1: {
    display: "flex",
    alignItems: "center",
    fontSize: "15px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "11px",
    },
  },
  tropy: {
    padding: "10px",
    display: "flex",
    justifyContent: "center",
    borderRadius:'50%'
  },
  offer:{
    fontSize:'15px',
    [theme.breakpoints.down("sm")]: {
      fontSize: "10px",
    },
  }
}));

export const Card = (props) => {
  const classes = useStyles(props);
  return (
    <div className={classes.root}>
      <Typography className={classes.tittle}>{props?.header}</Typography>

      {props.data.map((val) => {
        return (
          <>
            <div className={classes.main}>
              <div className={classes.section}>
                {val.type === "application" && (
                  <div
                    style={{
                      backgroundColor: "#F4F6F8",
                      padding: "10px",
                      borderRadius: "12px",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <img src={val.img} alt="application" />
                  </div>
                )}

                {val.type === "avatar" && (
                  <Avatar alt="Remy Sharp" src={val.img} />
                )}
                {val.type === "flag" && <img src={val.img} alt="flag" />}

                {/* tittle */}

                <div>
                  <Typography className={classes.tittle1}>
                    {val.tittle}
                  </Typography>
                  {/* devices  */}
                  {val.sub && (
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <span className={classes.sub}>
                        {val.device === "Window" ? (
                          <img src={val.icon} width="20px" height="20px" alt='img'/>
                        ) : (
                          val.icon
                        )}
                        &nbsp;{val.device}
                      </span>
                      {val.offer && (
                        <span
                          style={{
                            color: val.offer === "free" ? "green" : "red",
                            padding: "2px 6px",
                            backgroundColor:
                              val.offer === "free" ? "#E4F8DD" : "#FFE2E1",
                            borderRadius: "5px",
                            marginLeft: "10px",
                          }}
                          className={classes.offer}
                        >
                          {val.offer}
                        </span>
                      )}
                    </div>
                  )}
                </div>
              </div>
              <div>
                {val.type === "flag" && (
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      color: "#637381",
                      fontSize: "18px",
                    }}
                  >
                    <Grid container spacing={2}>
                      <Grid item xs={4}>
                      <div className={classes.flexBox1}>
                      {" "}
                      <AdbRoundedIcon />
                      &nbsp;{val.android}
                    </div>
                      </Grid>
                      <Grid item xs={4}>
                      <div className={classes.flexBox1}>
                      <img
                        src="https://img.icons8.com/windows/2x/windows8.png"
                        width="20px"
                        height="20px"
                        alt='img'
                      />
                      &nbsp;
                      {val.window}
                    </div>
                      </Grid>
                      <Grid item xs={4}>
                      <div className={classes.flexBox1}>
                      {" "}
                      <AppleIcon />
                      &nbsp;
                      {val.apple}
                    </div>
                      </Grid>
                    </Grid>
                  </div>
                )}
              </div>
              {val.likesec && (
                <>
                  <div>
                    <Rating
                      name="simple-controlled"
                      value={val?.value}
                      readOnly
                    />
                    <Typography className={classes.like}>
                      {val.like}&nbsp;reviews
                    </Typography>
                  </div>
                </>
              )}
              {val.cup && (
                <div
                  className={classes.tropy}
                  style={{ backgroundColor: val.color }}
                >
                  {val.tropy}
                </div>
              )}
            </div>
          </>
        );
      })}
    </div>
  );
};
