import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import { Box, Typography } from "@mui/material";
import TrendingUpIcon from "@mui/icons-material/TrendingUp";
import TrendingDownIcon from "@mui/icons-material/TrendingDown";
import BarChartIcon from "@mui/icons-material/BarChart";
// material UI styles
const useStyles = makeStyles((theme, props) => ({
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding:'25px',
    boxShadow:'0 27px 43px 0px #f2f3f5',
    border:'1px solid #f2f3f5',
    borderRadius:'12px',
  },
  stock: {
    width: "32px",
    height: "32px",
    display: "flex",
    backgroundColor: (props) => (props?.isStack ? "#E4F8DD" : "#FFE2E1"),
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "50%",
  },
  count: {
    fontWeight: 600,
    fontSize: "28px",
    marginTop: "10px",
  },

}));
// end
export const ActivityCard = (props) => {
  // use classes
  const classes = useStyles(props);
  return (
    <Box className={classes.root}>
      <Box>
        {/* tittle */}
        <Typography style={{ fontWeight: 600 }}>{props?.tittle}</Typography>
        <div
          style={{ display: "flex", alignItems: "center", marginTop: "10px" }}
        >
          <div className={classes.stock}>
            {/* stock up & down */}
            {props?.isStack ? (
              <TrendingUpIcon style={{ color: "green", fontSize: "20px" }} />
            ) : (
              <TrendingDownIcon style={{ color: "red", fontSize: "20px" }} />
            )}
          </div>
          {/* percentage */}
          <span style={{ marginLeft: "10px", fontWeight: 600 }}>{props?.data}%</span>
        </div>
        <Typography className={classes.count}>{props?.total}%</Typography>
      </Box>
      <Box>
        {/* chrt icon */}
        <BarChartIcon style={{color:props?.color , fontSize:'40px'}}/>
      </Box>
    </Box>
  );
};
