import { Grid } from "@mui/material";
import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import makeStyles from "@mui/styles/makeStyles";
import { Typography } from "@mui/material";
const useStyles = makeStyles((theme) => ({
  sliderconrant: {
    position: "absolute",
    bottom: "24px",
    left: "24px",
  },
  tittle: {
    color: "black",
    fontWeight: 600,
  },
  des: {
    color: "#ffff",
    fontWeight: 500,
    fontSize: "25px",
  },
  con: {
    marginTop: "10px",
    color: "#ffff",
    fontSize: "17px",
  },
}));
export const CarouselSliders = ({
  data = [],
  autoScroll = false,
  onClick = false,
}) => {
  var settings = {
    dots: false,
    infinite: true,
    // slidesToShow: 3,
    // slidesToScroll: 1,
    autoplay: autoScroll,
    speed: 1000,
    //     autoplaySpeed: 2000,
    cssEase: "linear",
    arrows: false,
  };
  const classes = useStyles();
  return (
    <div style={{ cursor: "pointer" }}>
      <Grid container justifyContent="center">
        <Grid item xs={12} alignItems={"center"}>
          <Slider {...settings}>
            {data.map((val) => {
              return (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    position: "relative",
                  }}
                  onClick={onClick ? onClick : false}
                >
                  <div
                    style={{
                      position: "relative",
                    }}
                  >
                    <img
                      src={val.img}
                      alt="img"
                      style={{
                        height: "317px",
                        width: "100%",
                        borderRadius: "12px",
                      }}
                    />
                    <div className={classes.sliderconrant}>
                      <Typography className={classes.tittle}>
                        {val.tittle}
                      </Typography>
                      <Typography className={classes.des}>{val.des}</Typography>
                      <Typography className={classes.con}>{val.con}</Typography>
                    </div>
                  </div>
                </div>
              );
            })}
          </Slider>
        </Grid>
      </Grid>
    </div>
  );
};
