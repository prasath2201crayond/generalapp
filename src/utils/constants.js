import SpeedIcon from "@mui/icons-material/Speed";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import EqualizerIcon from "@mui/icons-material/Equalizer";
import AccountBalanceOutlinedIcon from "@mui/icons-material/AccountBalanceOutlined";
import CalendarTodayOutlinedIcon from "@mui/icons-material/CalendarTodayOutlined";
import AppleIcon from "@mui/icons-material/Apple";
import FavoriteIcon from "@mui/icons-material/Favorite";
import EmojiEventsIcon from "@mui/icons-material/EmojiEvents";
/**
 * Object with role as key and value, which is used for
 * comparison of role in different place.
 */
export const UserRoles = {
  role: "role",
};

/**
 * Object which has the proper name of all the role
 * used in the application.
 */
export let UserRolesName = {
  role: "Role",
};

/**
 * Object which has the different themes used in
 * the application.
 */
export let Themes = {
  default: "default",
  dark: "dark",
};

/**
 * Object which has the different props for the Alert Component (/src/component/alert)
 * which is used via AlertContext (/src/contexts) and provided at /src/App.alert.js.
 */
export let AlertProps = {
  vertical: {
    top: "top",
    bottom: "bottom",
  },
  horizontal: {
    left: "left",
    right: "right",
    center: "center",
  },
  severity: {
    success: "success",
    error: "error",
    warning: "warning",
    info: "info",
  },
};

/**
 * Object which has the different props for the Drawer Component (/src/App.drawer.js)
 * which is used via DrawerContext (/src/contexts) and provided at /src/App.drawer.js.
 */
export const DrawerProps = {
  direction: {
    top: "top",
    bottom: "bottom",
    left: "left",
    right: "right",
  },
  variant: {
    permanent: "permanent",
    persistent: "persistent",
    temporary: "temporary",
  },
};

/**
 * Object has the key and value pair of all the keys which
 * are used to store some values in the local storage.
 */
export let LocalStorageKeys = {
  authToken: "auth_token",
  version: "version",
};

/**
 * Object has the key and value pair of all the HTTP method
 * used for an network call.
 */
export let NetWorkCallMethods = {
  get: "GET",
  post: "POST",
  put: "PUT",
  delete: "DELETE",
  update: "UPDATE",
};
// side navbar content
export let SideNavbarMenu = [
  {
    link: "/",
    tittle: "App",
    icon: <SpeedIcon style={{ opacity: 0.7 }} />,
  },
  {
    link: "/customers",
    tittle: "E-Commerce",
    icon: <LocalMallIcon style={{ opacity: 0.7 }} />,
  },
  {
    link: "/subscription",
    tittle: "Analytics",
    icon: <EqualizerIcon style={{ opacity: 0.7 }} />,
  },
  {
    link: "/plan",
    tittle: "Banking",
    icon: <AccountBalanceOutlinedIcon style={{ opacity: 0.7 }} />,
  },
  {
    link: "/addons",
    tittle: "Booking",
    icon: <CalendarTodayOutlinedIcon style={{ opacity: 0.7 }} />,
  },
];

export let RelatedCards = [
  {
    type: "application",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_chrome.svg",
    tittle: "Chrome",
    icon: <AppleIcon style={{ fontSize: "18px" }} />,
    device: "mac",
    offer: "free",
    like: "1.01k",
    sub: true,
    likesec: true,
    value:2,
  },
  {
    type: "application",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_drive.svg",
    tittle: "Drive",
    icon: <AppleIcon style={{ fontSize: "18px" }} />,
    device: "mac",
    offer: "$35.71",
    like: "99.3k",
    sub: true,
    likesec: true,
    value:2.5,
  },
  {
    type: "application",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_dropbox.svg",
    tittle: "Dropbox",
    icon: "https://img.icons8.com/windows/2x/windows8.png",
    device: "Window",
    offer: "free",
    like: "33.70k",
    sub: true,
    likesec: true,
    value:3,
  },
  {
    type: "application",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_evernote.svg",
    tittle: "Evernote",
    icon: <AppleIcon style={{ fontSize: "18px" }} />,
    device: "mac",
    offer: "$35.71",
    like: "99.3k",
    sub: true,
    likesec: true,
    value:1.5,
  },
  {
    type: "application",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_github.svg",
    tittle: "Github",
    icon: "https://img.icons8.com/windows/2x/windows8.png",
    device: "Window",
    offer: "$35.71",
    like: "99.3k",
    sub: true,
    likesec: true,
    value:2,
  },
];

export let Countries = [
  {
    type: "flag",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_flag_de.svg",
    tittle: "Germany",
    android: "68.29k",
    window: "67.66k",
    apple: "67.66k",
  },
  {
    type: "flag",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_flag_en.svg",
    tittle: "England",
    android: "63.90k",
    window: "76.28k",
    apple: "76.28k",
  },
  {
    type: "flag",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_flag_fr.svg",
    tittle: "France",
    android: "63.89k",
    window: "76.98k",
    apple: "77.89k",
  },
  {
    type: "flag",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_flag_kr.svg",
    tittle: "Korean",
    android: "76.78k",
    window: "36.40k",
    apple: "36.40k",
  },
  {
    type: "flag",
    img: "https://minimal-assets-api.vercel.app/assets/icons/ic_flag_us.svg",
    tittle: "USA",
    android: "60.13k",
    window: "20.89k",
    apple: "20.89k",
  },
];

export let AuthorsCard = [
  {
    type: "avatar",
    img: "https://minimal-assets-api.vercel.app/assets/images/avatars/avatar_2.jpg",
    tittle: "Lucian Obrien",
    icon: <FavoriteIcon style={{ fontSize: "18px" }} />,
    device: "15.97k",
    like: "1.01k",
    sub: true,
    cup: true,
    color: "#EBF8F2",
    tropy: <EmojiEventsIcon style={{ color: "#00AB55" }} />,
  },
  {
    type: "avatar",
    img: "https://minimal-assets-api.vercel.app/assets/images/avatars/avatar_3.jpg",
    tittle: "Deja Brady",
    icon: <FavoriteIcon style={{ fontSize: "18px" }} />,
    device: "15.85k",
    like: "99.3k",
    sub: true,
    cup: true,
    color: "#EDF6FF",
    tropy: <EmojiEventsIcon style={{ color: "#1890FF" }} />,
  },
  {
    type: "avatar",
    img: "https://minimal-assets-api.vercel.app/assets/images/avatars/avatar_1.jpg",
    tittle: "Jayvion Simon",
    icon: <FavoriteIcon style={{ fontSize: "18px" }} />,
    device: "15.97k",
    like: "11.33k",
    sub: true,
    cup: true,
    color: "#FFF1F0",
    tropy: <EmojiEventsIcon style={{ color: "#FF4842" }} />,
  },
];
